FROM openjdk:12-alpine
WORKDIR /app
COPY build/libs/cars-api.jar /app/cars-api.jar
CMD ["java", "-jar", "cars-api.jar"]